
from django.db import models
from django.contrib.auth.models import User
class Customer(models.Model):
    firstname =models.CharField(max_length=20)
    lastname=models.CharField(max_length=20)
    dateOfBirth=models.DateField()
    nationality=models.CharField(max_length=20)
    district=models.CharField(max_length=20)
    def __str__(self):
        return self.firstname
  #creating a model which allows users to upload their profile pics  
class Profile(models.Model):
    user=models.OneToOneField(User, on_delete=models.CASCADE)
    image=models.ImageField(default='default.jpg', upload_to='profile_pics')
    def __str__(self):#dander str method
        return f'{self.user.username} Profile'
    
    #register every model you create within the admin page
# Create your models here.
